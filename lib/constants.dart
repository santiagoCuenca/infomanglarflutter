
class Constants {


  static String version = "1.0.9";

  //static String hostUrl = "http://10.0.2.2:8080/info-manglar/"; // LOCAL
  //static String hostUrl = "http://localhost:8080/info-manglar/"; // WEB LOCAL
  static bool isWeb = true;
  static String hostUrl = "http://23.20.94.236/info-manglar/"; // PROD


  // MANGLARAPP
//  static int colorPrimary = 0xff4C990B;
//  static int colorSecondary = 0xff1E6561;
//  static int colorDark = 0xff232222;
//  static int colorTertiary = 0xFF33B878;
//  static bool simpleHeader = false;
//  static String configPath = "assets/configManglarApp/";
//  static String imagesPath = "assets/imagesManglarApp/";

  // INFOMANGLAR
  static int colorPrimary = 0xffA95D2F;
  static int colorSecondary = 0xff7A6144;
  static int colorDark = 0xff232222;
  static int colorLight = 0xffe6d0c3;
  static int colorTertiary = 0xFF33B878;
  static bool simpleHeader = true;
  static String configPath = "assets/config/";
  static String imagesPath = "assets/images/";

}
